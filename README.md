# gosrt

gosrt sorts top-level symbols in [Go] code.

> ⚠ gosrt is still in early development. It might work, it might destroy your
> code! Only use it on code under version control.

The order is the same as that of [godoc], i.e:

1.  Package declarations, imports, ‘loose’ comments
2.  Constants
3.  Variables
4.  Functions (except constructors), alphabetically
5.  Types:
    1.  Type declaration
    2.  Constructors, alphabetically
    3.  Methods, alphabetically

## 💾 Installation

```sh
go install gitlab.com/biffen/gosrt@latest
```

## ⌨ Usage

See [usage.txt].

## Example

```go
package example

func Zebra() { /* … */ }

func NewOcelot() (*Ocelot, error) { /* … */ }

type Ocelot struct { /* … */ }

func (o *Ocelot) Close() error { /* … */ }

var (
    global = 42
)

func Albatross() { /* … */ }

func (o *Ocelot) String() string { /* … */ }
```

```sh
gosrt -w example.go
```

```go
package example

var (
    global = 42
)

func Albatross() { /* … */ }

func Zebra() { /* … */ }

type Ocelot struct { /* … */ }

func NewOcelot() (*Ocelot, error) { /* … */ }

func (o *Ocelot) Close() error { /* … */ }

func (o *Ocelot) String() string { /* … */ }
```

## 🪲 Bugs

If you find a bug don’t be surprised. But please report it at
<https://gitlab.com/biffen/gosrt/issues>.

## License

gosrt is licensed under [the MIT license].

[go]: https://golang.org
[godoc]: https://godoc.org/golang.org/x/tools/cmd/godoc
[the mit license]: https://opensource.org/licenses/MIT
[usage.txt]: ./usage.txt
