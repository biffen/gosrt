module gitlab.com/biffen/gosrt

go 1.23

require (
	github.com/stretchr/testify v1.9.0
	gitlab.com/biffen/go-applause v0.5.0
	golang.org/x/exp v0.0.0-20241009180824-f66d83c29e7c
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/kr/text v0.2.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gitlab.com/biffen/error v0.2.0 // indirect
	gitlab.com/biffen/typo v0.0.1 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
