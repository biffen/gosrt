/*
Package gosrt sorts top-level symbols in Go code.

For usage documentation see <https://gitlab.com/biffen/gosrt> or `godoc -h`.
*/
package main
