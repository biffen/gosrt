//nolint:testpackage // Tests non-exported
package internal

import (
	"context"
	"fmt"
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func ExampleBaseFile_Diff() {
	if err := os.Setenv("TZ", "UTC0"); err != nil {
		panic(err)
	}

	file := &BaseFile{
		after: []byte(`foo
baz
`),
		before: []byte(`foo
bar
`),
		mtime: time.Unix(0, 0),
		name:  "example",
	}

	diff, err := file.Diff(context.Background(), file.mtime)
	if err != nil {
		panic(err)
	}

	fmt.Println(diff)

	//nolint:godot // Come on!
	// Output:
	// diff -u example.orig example
	// --- example.orig	1970-01-01 00:00:00.000000000 +0000
	// +++ example	1970-01-01 00:00:00.000000000 +0000
	// @@ -1,2 +1,2 @@
	//  foo
	// -bar
	// +baz
}

func TestAppendFile(t *testing.T) {
	t.Parallel()

	stdin, err := NewStdFile()
	require.NoError(t, err)

	file1, err := NewFsFile("file.go")
	require.NoError(t, err)

	file2, err := NewFsFile("file_test.go")
	require.NoError(t, err)

	for _, c := range [...]struct {
		a        []File
		b        File
		expected []File
	}{
		{[]File{}, stdin, []File{stdin}},
		{[]File{}, file2, []File{file2}},
		{[]File{file2}, stdin, []File{stdin, file2}},
		{[]File{file2}, file1, []File{file1, file2}},
	} {
		c := c

		t.Run(c.b.String(), func(t *testing.T) {
			t.Parallel()

			actual := AppendFile(c.a, c.b)
			assert.Equal(t, c.expected, actual)
		})
	}
}
