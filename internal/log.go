package internal

import (
	"context"
	"fmt"
	"log/slog"
	"os"
	"sync"
)

var _ slog.Handler = (*Handler)(nil)

type Handler struct {
	attributes []slog.Attr
	groups     []string
	mu         sync.Mutex
	slog.Level
}

func NewHandler(level slog.Level) *Handler {
	return &Handler{
		Level: level,
	}
}

func (h *Handler) Enabled(_ context.Context, level slog.Level) bool {
	return level >= h.Level
}

func (h *Handler) Handle(_ context.Context, record slog.Record) error {
	record.AddAttrs(h.attributes...)

	h.mu.Lock()
	defer h.mu.Unlock()

	fmt.Fprintf(os.Stderr, "gosrt: %s %s", record.Level, record.Message)

	record.Attrs(func(attr slog.Attr) bool {
		fmt.Fprintf(os.Stderr, " [%s=%q]", attr.Key, attr.Value)
		return true
	})

	fmt.Fprintln(os.Stderr)

	return nil
}

func (h *Handler) WithAttrs(attrs []slog.Attr) slog.Handler {
	if len(attrs) == 0 {
		return h
	}

	return &Handler{
		Level:      h.Level,
		attributes: append(h.attributes, attrs...),
		groups:     h.groups,
	}
}

func (h *Handler) WithGroup(name string) slog.Handler {
	if name == "" {
		return h
	}

	return &Handler{
		Level:      h.Level,
		attributes: h.attributes,
		groups:     append(h.groups, name),
	}
}
