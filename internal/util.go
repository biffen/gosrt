package internal

import (
	"go/ast"
)

// FilterFileComments removes each comment from a file’s AST for which the
// supplied filter returns true. The removed comments are returned.
func FilterFileComments(
	file *ast.File,
	filter func(*ast.CommentGroup) bool,
) (removed []*ast.CommentGroup) {
	var i int

	for _, comment := range file.Comments {
		if comment == nil {
			continue
		}

		if filter(comment) {
			removed = append(removed, comment)

			continue
		}

		file.Comments[i] = comment
		i++
	}

	file.Comments = file.Comments[:i]

	return
}

// FilterFileDecls removes each declaration from a file’s AST for which the
// supplied filter returns true.
func FilterFileDecls(file *ast.File, filter func(ast.Decl) bool) {
	var i int

	for _, decl := range file.Decls {
		if filter(decl) {
			continue
		}

		file.Decls[i] = decl
		i++
	}

	file.Decls = file.Decls[:i]
}

// Within returns true if an AST node is within an other node.
func Within(inner, outer ast.Node) bool {
	return inner != nil &&
		outer != nil &&
		inner.Pos() >= outer.Pos() &&
		inner.End() <= outer.End()
}
