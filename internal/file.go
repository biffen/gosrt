package internal

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"time"
)

var (
	_ File         = (*FsFile)(nil)
	_ File         = (*StdFile)(nil)
	_ fmt.Stringer = (*BaseFile)(nil)
	_ fmt.Stringer = (*FsFile)(nil)
	_ fmt.Stringer = (*StdFile)(nil)
)

// BaseFile has common fields and methods for file-like things.
type BaseFile struct {
	mtime         time.Time
	name          string
	after, before []byte
}

// After returns the file’s contents after it is transformed.
func (file *BaseFile) After() []byte {
	return file.after
}

// Changed returns true if the file changed when it was transformed.
func (file *BaseFile) Changed() bool {
	return !bytes.Equal(file.before, file.After())
}

// Diff uses diff(1) to print the differences between the original file and the
// same file transformed.
func (file *BaseFile) Diff(ctx context.Context, now time.Time) (string, error) {
	var tmps [2]string

	for i, b := range [][]byte{file.before, file.After()} {
		f, err := os.CreateTemp("", ".gosrt-tmp-")
		if err != nil {
			return "",
				fmt.Errorf("failed to create temporary file for diff: %w", err)
		}

		tmp := f.Name()

		//nolint:errcheck // Not much to do.
		defer os.Remove(tmp)

		err = os.WriteFile(tmp, b, 0)
		if err != nil {
			return "", fmt.Errorf(
				"failed to write to temporary file for diff: %w",
				err,
			)
		}

		tmps[i] = tmp
	}

	for i, t := range [...]time.Time{
		file.mtime,
		now,
	} {
		if err := os.Chtimes(tmps[i], time.Time{}, t); err != nil {
			return "", fmt.Errorf(
				"failed to change mtime of temporary file for diff: %w",
				err,
			)
		}
	}

	var (
		buffer bytes.Buffer
		//nolint:gosec // Not dangerous.
		cmd = exec.CommandContext(ctx, "diff", "-u", tmps[0], tmps[1])
	)

	fmt.Fprintf(&buffer, "diff -u %s.orig %s\n", file, file)

	cmd.Stdout = &buffer

	err := cmd.Run()

	if buffer.Len() == 0 {
		return "", fmt.Errorf("failed to run diff: %w", err)
	}

	b := buffer.Bytes()

	b = bytes.Replace(b,
		[]byte("--- "+tmps[0]+"\t"),
		[]byte(fmt.Sprintf("--- %s.orig\t", file)),
		1)
	b = bytes.Replace(b,
		[]byte("+++ "+tmps[1]+"\t"),
		[]byte(fmt.Sprintf("+++ %s\t", file)),
		1)

	return string(b), nil
}

// Print prints the transformed file’s contents.
func (file *BaseFile) Print() error {
	//nolint:forbidigo // Meh.
	_, err := fmt.Print(string(file.After()))

	//nolint:wrapcheck // No need.
	return err
}

func (file *BaseFile) String() string {
	return file.name
}

// Transform changes the file’s contents.
func (file *BaseFile) Transform(
	_ context.Context,
	transformer Transformer,
) error {
	after, err := transformer(file.before)
	if err != nil {
		return err
	}

	file.after = after

	return nil
}

// File is something file-like.
type File interface {
	fmt.Stringer

	After() []byte
	Backup(suffix string) error
	Changed() bool
	Compare(other File) int
	Diff(ctx context.Context, now time.Time) (string, error)
	Print() error
	Transform(ctx context.Context, transformer Transformer) error
	Write() error
}

// AppendFile inserts a File into its sorted position in a slice of Files.
func AppendFile(a []File, b File) []File {
	var i int
LOOP:
	for j, f := range a {
		c := f.Compare(b)
		switch {
		case c > 0:
			i = j

			break LOOP

		case c == 0:
			return a
		}
	}

	return append(a[:i], append([]File{b}, a[i:]...)...)
}

// NewFile returns a new file based on a string. "-" is an [StdFile], all other
// strings are paths for [FsFile]s.
func NewFile(name string) (file File, err error) {
	if name == "-" {
		return NewStdFile()
	}

	return NewFsFile(name)
}

// FsFile is a file in a file system.
type FsFile struct {
	info os.FileInfo
	BaseFile
}

// NewFsFile returns a new [FsFile] for a path.
func NewFsFile(path string) (*FsFile, error) {
	path = filepath.Clean(path)

	info, err := os.Stat(path)
	if err != nil {
		return nil, fmt.Errorf("failed to stat %q: %w", path, err)
	}

	b, err := os.ReadFile(path)
	if err != nil {
		return nil, fmt.Errorf("failed to read file %q: %w", path, err)
	}

	return &FsFile{
		BaseFile: BaseFile{
			before: b,
			mtime:  info.ModTime(),
			name:   path,
		},
		info: info,
	}, nil
}

// Backup makes a copy of a file with the specified suffix.
func (file *FsFile) Backup(suffix string) error {
	backup := file.name + suffix

	if _, err := os.Stat(backup); !os.IsNotExist(err) {
		return fmt.Errorf("failed to create backup; %q exists: %w", backup, err)
	}

	if err := os.WriteFile(
		backup,
		file.before,
		file.info.Mode(),
	); err != nil {
		return fmt.Errorf(
			"failed to back up %q to %q: %w",
			file,
			backup,
			err,
		)
	}

	return nil
}

// Compare compares an [FsFile] to an other File for sorting.
func (file *FsFile) Compare(other File) int {
	if other, ok := other.(*FsFile); ok {
		return strings.Compare(file.name, other.name)
	}

	return 1
}

func (file *FsFile) Write() error {
	if !file.Changed() {
		return nil
	}

	if err := os.WriteFile(file.name, file.After(), 0); err != nil {
		return fmt.Errorf(
			"failed to write sorted file back to %q: %w",
			file,
			err,
		)
	}

	return nil
}

// StdFile is a ‘file’ that is read from STDIN and printed to STDOUT.
type StdFile struct {
	BaseFile
}

// NewStdFile returns a new [StdFile] whose contents are read from STDIN.
func NewStdFile() (*StdFile, error) {
	b, err := io.ReadAll(os.Stdin)
	if err != nil {
		return nil, fmt.Errorf("failed to read STDIN: %w", err)
	}

	return &StdFile{
		BaseFile: BaseFile{
			before: b,
			mtime:  time.Now(),
			name:   "<standard input>",
		},
	}, nil
}

// Backup does nothing.
func (file *StdFile) Backup(string) error {
	return nil
}

// Compare compares this [StdFile] to an other [File] for sorting. It returns 0
// for other [StdFiles] and -1 for all other files.
func (file *StdFile) Compare(other File) int {
	if _, ok := other.(*StdFile); ok {
		return 0
	}

	return -1
}

func (file *StdFile) Write() error {
	return file.Print()
}

// Transformer is a function that takes bytes, transforms them and returns them.
type Transformer func([]byte) ([]byte, error)
