package internal

import (
	"bytes"
	"context"
	"fmt"
	"go/ast"
	"go/doc"
	"go/format"
	"go/parser"
	"go/printer"
	"go/token"
	"log/slog"
)

const commentsKey = "comments"

// Sorter can sort a Go file.
type Sorter struct {
	file   *ast.File
	fset   *token.FileSet
	sorted []*printer.CommentedNode
}

// NewSorter returns a new Sorter for some source code. The source is passed on
// to [go/parser.ParseFile].
func NewSorter(source interface{}) (*Sorter, error) {
	s := &Sorter{
		fset: token.NewFileSet(),
	}

	file, err := parser.ParseFile(s.fset, "", source, parser.ParseComments)
	if err != nil {
		return nil, fmt.Errorf("failed to parse source: %w", err)
	}

	s.file = file

	return s, nil
}

// Sort sorts the file and returns the sorted contents.
func (s *Sorter) Sort(ctx context.Context) ([]byte, error) {
	//nolint:staticcheck // Fix in the future
	pak := doc.New(&ast.Package{
		Name: s.file.Name.Name,
		Files: map[string]*ast.File{
			"": s.file,
		},
	}, "", doc.AllDecls|doc.AllMethods|doc.PreserveAST)

	for _, c := range pak.Consts {
		s.sortNode(ctx, c)
	}

	for _, v := range pak.Vars {
		s.sortNode(ctx, v)
	}

	for _, f := range pak.Funcs {
		s.sortNode(ctx, f)
	}

	for _, t := range pak.Types {
		s.sortNode(ctx, t)

		for _, f := range t.Funcs {
			s.sortNode(ctx, f)
		}

		for _, m := range t.Methods {
			if m.Level > 0 {
				// Don’t repeat embedded methods.
				continue
			}

			s.sortNode(ctx, m)
		}
	}

	var buffer bytes.Buffer

	if err := printer.Fprint(&buffer, s.fset, s.file); err != nil {
		panic(err)
	}

	for _, cn := range s.sorted {
		fmt.Fprint(&buffer, "\n\n")

		if err := printer.Fprint(&buffer, s.fset, cn); err != nil {
			return nil, fmt.Errorf("error when printing sorted node: %w", err)
		}
	}

	pretty, err := format.Source(buffer.Bytes())
	if err != nil {
		return nil, fmt.Errorf("failed to format code: %w", err)
	}

	if _, err = parser.ParseFile(token.NewFileSet(),
		"",
		pretty,
		parser.DeclarationErrors|parser.AllErrors); err != nil {
		return nil,
			fmt.Errorf("internal error: sorted file does not parse (%w)", err)
	}

	return pretty, nil
}

func (s *Sorter) sortNode(ctx context.Context, node any) {
	if node == nil {
		return
	}

	var (
		cg   *ast.CommentGroup
		decl ast.Node
	)

	switch t := node.(type) {
	case *doc.Func:
		cg = t.Decl.Doc
		decl = t.Decl

	case *doc.Type:
		cg = t.Decl.Doc
		decl = t.Decl

	case *doc.Value:
		cg = t.Decl.Doc
		decl = t.Decl
	}

	cn := &printer.CommentedNode{
		Node: decl,
	}

	if cg != nil && len(cg.List) > 0 {
		slog.DebugContext(ctx, "Appending doc comment",
			slog.Int(commentsKey, len(cg.List)),
		)

		cn.Comments = append(cn.Comments, cg)

		for _, a := range cg.List {
			a := a

			FilterFileComments(s.file, func(b *ast.CommentGroup) bool {
				return a.Pos() == b.Pos()
			})
		}
	}

	FilterFileDecls(s.file, func(b ast.Decl) bool {
		// Special case because doc splits `type ( … )` into separate nodes.
		if gd, ok := b.(*ast.GenDecl); ok &&
			gd.Tok == token.TYPE &&
			Within(decl, b) {
			slog.DebugContext(ctx, "Decl within type block")
			return true
		}

		return Within(b, decl)
	})

	if comments := FilterFileComments(s.file, func(b *ast.CommentGroup) bool {
		return Within(b, decl)
	}); len(comments) > 0 {
		slog.DebugContext(ctx, "Comments within node",
			slog.Int(commentsKey, len(comments)),
		)

		cn.Comments = append(cn.Comments, comments...)
	}

	if genDecl, ok := decl.(*ast.GenDecl); ok {
		slog.DebugContext(ctx, "Node is a GenDecl",
			slog.Int("specs", len(genDecl.Specs)),
		)
		var comments []*ast.CommentGroup

		for _, s := range genDecl.Specs {
			var comment *ast.CommentGroup
			switch t := s.(type) {
			case *ast.ValueSpec:
				slog.DebugContext(ctx, "Value spec",
					slog.Any("names", t.Names),
				)
				comment = t.Comment

			case *ast.TypeSpec:
				slog.DebugContext(ctx, "Type spec",
					slog.Any("name", t.Name),
				)

				comment = t.Comment
			}
			if comment == nil {
				slog.DebugContext(ctx, "Spec has no comment")

				continue
			}

			comments = append(comments, comment)
		}

		if filtered := FilterFileComments(s.file,
			func(b *ast.CommentGroup) bool {
				for _, a := range comments {
					if a.Pos() == b.Pos() {
						return true
					}
				}

				return false
			}); len(filtered) > 0 {
			slog.DebugContext(ctx, "GenDecl comments",
				slog.Int(commentsKey, len(comments)),
				slog.Int("filtered", len(filtered)),
			)

			cn.Comments = append(cn.Comments, filtered...)
		} else {
			slog.DebugContext(ctx, "GenDecl had no comments")
		}
	}

	if cn.Node == nil {
		slog.DebugContext(ctx, "No node to append")
		return
	}

	if slog.Default().Enabled(ctx, slog.LevelDebug) {
		var v slog.Value
		if n, ok := cn.Node.(ast.Node); ok {
			v = slog.GroupValue(
				slog.String("type", fmt.Sprintf("%T", cn.Node)),
				slog.Any("pos", n.Pos()),
				slog.Any("end", n.End()),
			)
		} else {
			v = slog.AnyValue(cn.Node)
		}

		slog.DebugContext(ctx, "Appending node",
			slog.Int(commentsKey, len(cn.Comments)),
			slog.Any("node", v),
		)
	}

	s.sorted = append(s.sorted, cn)
}
