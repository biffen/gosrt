# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog] and this project adheres to [Semantic
Versioning].

## Unreleased

### Changed

-   Logging format and a new `-q` option

## 0.4.2 - 2023-04-19

### Fixed

-   Bug when specs have no comments

## 0.4.1 - 2023-03-26

### Changed

-   Better usage text

### Fixed

-   Don’t duplicate some trailing comments

## v0.4.0 - 2022-11-08

Internal changes only.

## v0.3.0 - 2021-03-06

### Changed

-   Moved the project

## 0.2.0 - 2020-10-02

Internal changes only.

## 0.1.0 - 2020-03-04

Internal changes only.

## 0.0.1 - 2019-12-18

Initial implementation

[Keep a Changelog]: https://keepachangelog.com
[Semantic Versioning]: https://semver.org
