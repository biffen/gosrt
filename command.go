package main

import (
	"context"
	_ "embed"
	"fmt"
	"log/slog"
	"os"
	"path/filepath"
	"regexp"
	"strings"
	"time"

	"gitlab.com/biffen/go-applause"
	"gitlab.com/biffen/gosrt/internal"
)

const (
	// ExitCancelled is the exit code for when the process is cancelled, i.e.
	// killed.
	ExitCancelled = 3

	// ExitDiff is the exit code for when files diff from their sorted state.
	ExitDiff = 1

	// ExitError is the exit code for any runtime error.
	ExitError = 4

	// ExitUser is the exit code for user errors.
	ExitUser = 2
)

var (
	generatedPattern = regexp.MustCompile(
		`^// Code generated .* DO` + ` NOT EDIT\.$`,
	)
	//go:embed usage.txt
	usage string
)

// NewCommand returns a new [Command] for a list of arguments.
func NewCommand(
	ctx context.Context,
	args []string,
) (func(context.Context) (int, error), error) {
	var (
		c = &Command{
			Backup: true,
		}
		help   bool
		level  = slog.LevelWarn
		parser applause.Parser
	)

	parser.Add(
		applause.Option{
			Names:  []string{"h", "help"},
			Target: &help,
		},
		applause.Option{
			Names:  []string{"backup", "b"},
			Negate: true,
			Target: &c.Backup,
		},
		applause.Option{
			Names:  []string{"diff", "d"},
			Target: &c.Diff,
		},
		applause.Option{
			Names:  []string{"list", "l"},
			Target: &c.List,
		},
		applause.Option{
			Add:    +4,
			Names:  []string{"quiet", "q"},
			Target: &level,
		},
		applause.Option{
			Add:    -4,
			Names:  []string{"verbose", "v"},
			Target: &level,
		},
		applause.Option{
			Names:  []string{"write", "w"},
			Negate: true,
			Target: &c.Write,
		},
	)

	operands, err := parser.Parse(ctx, args)
	if err != nil {
		return nil, fmt.Errorf("user error: %w", err)
	}

	if help {
		return func(context.Context) (int, error) {
			if _, err := fmt.Fprint(os.Stdout, usage); err != nil {
				panic(err)
			}

			return 0, nil
		}, nil
	}

	slog.SetDefault(slog.New(internal.NewHandler(level)))

	if len(operands) == 0 {
		operands = []string{"-"}
	}

	for _, operand := range operands {
		info, err := os.Stat(operand)
		if err == nil &&
			info != nil &&
			info.IsDir() {
			if err = c.findFiles(ctx, operand); err != nil {
				return nil, err
			}

			continue
		}

		file, err := internal.NewFile(operand)
		if err != nil {
			return nil, fmt.Errorf("failed to read file %q: %w", operand, err)
		}

		c.Files = internal.AppendFile(c.Files, file)
	}

	slog.InfoContext(ctx, "Sorting files",
		slog.Int("files", len(c.Files)))

	return c.run, nil
}

// Command is a gosrt invocation.
type Command struct {
	Files  []internal.File
	Backup bool
	Diff   bool
	List   bool
	Write  bool
}

func (c *Command) findFiles(ctx context.Context, dir string) error {
	if err := filepath.Walk(
		dir,
		func(path string, info os.FileInfo, err error) error {
			logger := slog.With(slog.String("path", path))

			switch {
			case err != nil:
				return err

			case info.IsDir():
				if info.Name() == "vendor" ||
					(len(info.Name()) > 1 &&
						strings.HasPrefix(info.Name(), ".")) {
					logger.InfoContext(ctx, "Skipping directory")

					return filepath.SkipDir
				}

				return nil

			case strings.HasSuffix(path, ".go"):
				if strings.HasPrefix(info.Name(), ".") {
					logger.InfoContext(ctx, "Skipping file")

					return nil
				}

				file, err := internal.NewFsFile(path)
				if err != nil {
					//nolint:wrapcheck // No need.
					return err
				}

				if generatedPattern.Match(file.After()) {
					logger.InfoContext(ctx, "Skipping generated file")

					return nil
				}

				logger.InfoContext(ctx, "Found file")

				c.Files = internal.AppendFile(c.Files, file)

				return nil
			}

			return nil
		},
	); err != nil {
		return fmt.Errorf("failed to find files in %q: %w", dir, err)
	}

	return nil
}

func (c *Command) run(ctx context.Context) (exitCode int, err error) {
	now := time.Now()

	for _, file := range c.Files {
		logger := slog.With(slog.Any("file", file))

		select {
		case <-ctx.Done():
			exitCode = ExitCancelled
			err = ctx.Err()

			return
		default:
		}

		logger.InfoContext(ctx, "Working on file")

		if err = file.Transform(ctx,
			func(source []byte) (dest []byte, err error) {
				return Sort(ctx, source)
			}); err != nil {
			err = fmt.Errorf("failed to sort file %q: %w", file, err)

			return
		}

		switch {
		case c.Diff:
			if file.Changed() {
				exitCode = ExitDiff

				var diff string

				diff, err = file.Diff(ctx, now)
				if err != nil {
					err = fmt.Errorf("computing diff failed: %w", err)

					return
				}

				//nolint:forbidigo // Meh.
				fmt.Println(diff)
			} else {
				logger.InfoContext(ctx, "no diff")
			}

		case c.List:
			if file.Changed() {
				exitCode = ExitDiff

				//nolint:forbidigo // Meh.
				fmt.Println(file)
			}

		case c.Write:
			if file.Changed() {
				logger.InfoContext(ctx, "File changed")

				err = file.Backup(now.Format("~gosrt-200601021504050700~"))
				if err == nil {
					err = file.Write()
				}

				if err != nil {
					err = fmt.Errorf("failed to write file %q: %w", file, err)

					return
				}

				logger.InfoContext(ctx, "Wrote sorted file")
			} else {
				logger.InfoContext(ctx, "File unchanged")
			}

		default:
			err = file.Print()
			if err != nil {
				err = fmt.Errorf("failed to print file %q: %w", file, err)

				return
			}
		}
	}

	return
}
