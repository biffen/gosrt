package main

import (
	"context"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func ExampleCommand() {
	tmp, err := os.CreateTemp("", "")
	if err != nil {
		panic(err)
	}

	defer func() {
		if err = os.Remove(tmp.Name()); err != nil {
			panic(err)
		}
	}()

	if _, err = tmp.Write([]byte(`package main

func b() {}

func a() {}
`)); err != nil {
		panic(err)
	}

	ctx := context.Background()

	cmd, err := NewCommand(ctx, []string{"-v", tmp.Name()})
	if err != nil {
		panic(err)
	}

	_, err = cmd(ctx)
	if err != nil {
		panic(err)
	}

	//nolint:godot // Come on!
	// Output:
	// package main
	//
	// func a() {}
	//
	// func b() {}
}

func TestCommand_findFiles(t *testing.T) {
	t.Parallel()

	var (
		c   Command
		ctx = context.Background()
	)

	err := c.findFiles(ctx, ".")
	require.NoError(t, err)

	assert.NotEmpty(t, c.Files)
}

func TestNewCommand(t *testing.T) {
	t.Parallel()

	for _, c := range [...]struct {
		Name  string
		Args  []string
		Error string
	}{
		{
			Name: "No args",
			Args: []string{},
		},

		{
			Name: "-h",
			Args: []string{"-h"},
		},

		{
			Name:  "Unknown option",
			Args:  []string{"--unknown"},
			Error: `unknown option\b.*--unknown`,
		},

		{
			Name:  "Missing file",
			Args:  []string{"a file that shouldn’t exist"},
			Error: `no such file or directory`,
		},
	} {
		c := c

		t.Run(c.Name, func(t *testing.T) {
			t.Parallel()

			var (
				ctx      = context.Background()
				cmd, err = NewCommand(ctx, c.Args)
			)

			if c.Error != "" {
				assert.Nil(t, cmd)
				require.Error(t, err)
				assert.Regexp(t, c.Error, err.Error())

				return
			}

			assert.NotNil(t, cmd)
			assert.NoError(t, err)
		})
	}
}
