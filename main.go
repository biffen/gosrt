// Package main is the code that runs the gosrt utility.
package main

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"syscall"
)

func main() {
	var (
		ctx, stop = signal.NotifyContext(
			context.Background(),
			os.Interrupt,
			syscall.SIGTERM,
		)
		exit int
	)

	defer func() { os.Exit(exit) }()
	defer stop()

	cmd, err := NewCommand(ctx, os.Args[1:])
	if err != nil {
		exit = ExitUser

		return
	}

	exit, err = cmd(ctx)
	if err != nil {
		fmt.Fprintf(os.Stderr, "ERROR: %v\n", err)

		if exit == 0 {
			exit = ExitError
		}
	}
}
