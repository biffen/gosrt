package main

import (
	"context"

	"gitlab.com/biffen/gosrt/internal"
)

// Sort sorts Go code. The input is anything that [go/parser.ParseFile]
// understands. The output is source code as bytes.
func Sort(ctx context.Context, source any) ([]byte, error) {
	s, err := internal.NewSorter(source)
	if err != nil {
		//nolint:wrapcheck // No need.
		return nil, err
	}

	//nolint:wrapcheck // No need.
	return s.Sort(ctx)
}
