package main_test

import (
	"context"
	"embed"
	"errors"
	"go/format"
	"io/fs"
	"path/filepath"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	gosrt "gitlab.com/biffen/gosrt"
)

//go:embed testdata/*.error
//go:embed testdata/*.expected
//go:embed testdata/*.input
var testdata embed.FS

func TestSort(t *testing.T) {
	t.Parallel()

	ctx := context.Background()

	files, err := testdata.ReadDir("testdata")
	require.NotEmpty(t, files)
	require.NoError(t, err)

	for _, file := range files {
		file := file

		if !strings.HasSuffix(file.Name(), ".input") {
			continue
		}

		var (
			base = strings.TrimSuffix(file.Name(), ".input")
			read = func(name string) ([]byte, error) {
				return testdata.ReadFile(filepath.Join("testdata", name))
			}
		)

		t.Run(base, func(t *testing.T) {
			t.Parallel()

			input, err := read(file.Name())
			require.NoError(t, err)

			var expectError bool

			expected, err := read(base + ".expected")
			if errors.Is(err, fs.ErrNotExist) {
				expected, err = read(base + ".error")
				expectError = true
			}
			require.NoError(t, err)

			actual, err := gosrt.Sort(ctx, input)

			if expectError {
				assert.Error(t, err)
				assert.ErrorContains(
					t,
					err,
					strings.TrimSpace(string(expected)),
				)

				return
			}

			require.NoError(t, err)

			expected, err = format.Source(expected)
			require.NoError(t, err)

			assert.Equal(t, string(expected), string(actual))
		})
	}
}
